var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
// Lanjutkan code untuk menjalankan function readBooksPromise 
var timeAvailable = 10000
var i = 0
function readBooks(){
    readBooksPromise(timeAvailable, books[i])
    .then(function(fullfiled){
        i++
        console.log(fullfiled)
        timeAvailable = fullfiled
        if (books[i]) {
            readBooks()
        }
    })
    .catch(function(error){
        console.log(error.message)
    })
}

readBooks()