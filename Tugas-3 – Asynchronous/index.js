var readBooks = require('./callback.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
// Tulis code untuk memanggil function readBooks di sini
var timeAvailable = 10000
var i = 0

readBooks(timeAvailable, books[i], function(timeAvailable) {
    if(timeAvailable){
        i++
        readBooks(timeAvailable, books[i], function(timeAvailable){
            if(timeAvailable){
                i++
                readBooks(timeAvailable, books[i], function(timeAvailable){
                    if(timeAvailable){
                        console.log("Waktu membaca tersisa " + timeAvailable)
                    } else {
                        console.log("Waktu membaca Habis")
                    }
                })
            }
        })
    }
})