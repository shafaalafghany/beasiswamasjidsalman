function readBooks(time, book, callback ) {
    console.log(`saya membaca ${book.name}`)
    setTimeout(function(){
        let sisaWaktu = 0
        if(time > book.timeSpent) {
            sisaWaktu = time - book.timeSpent
            console.log(`saya sudah membaca ${book.name}, sisa waktu saya ${sisaWaktu}`)
            callback(sisaWaktu) //menjalankan function callback
        } else {
            console.log('waktu saya habis')
            callback(time)
        }   
    }, book.timeSpent)
}
var timeAvailable = 10000
var i = 0
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]

// readBooks(timeAvailable, books[i], function(timeAvailable) {
//     if(timeAvailable){
//         i++
//         readBooks(timeAvailable, books[i], function(timeAvailable){
//             if(timeAvailable){
//                 i++
//                 readBooks(timeAvailable, books[i], function(timeAvailable){
//                     if(timeAvailable){
//                         console.log("Waktu membaca tersisa " + timeAvailable)
//                     } else {
//                         console.log("Waktu membaca Habis")
//                     }
//                 })
//             }
//         })
//     }
// })

 
module.exports = readBooks 