import React, { useState, createContext } from "react"
import Todolist from './Todolist'

export const RootContext = createContext()


const Context = () => {

    const [ input, setInput ] = useState('')
    const [ todos, setTodos ] = useState([])
    var [ index, setIndex ] = useState(0)

    handleChangeInput = (value) => {
        setInput(value)
    }

    addTodo = () => {
        const day = new Date().getDate()
        const month = new Date().getMonth()
        const year = new Date().getFullYear()

        const today = `${day}/${month+1}/${year}`
        setTodos([...todos, { index : index, title: input, date: today }])
        setInput('')
        setIndex(++index)
    }

    deleteTodo = (key) => {
        var list = todos.filter(item => item.index != key)
        setTodos(list)

    }

    return (
        <RootContext.Provider value={{
            input,
            todos,
            handleChangeInput,
            addTodo,
            deleteTodo,
        }}>
            <Todolist />
        </RootContext.Provider>
    )
}

export default Context