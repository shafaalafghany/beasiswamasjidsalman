import { blue } from "ansi-colors"
import React, { createContext, useContext } from "react"
import {
  View,
  Image,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  FlatList,
} from "react-native"
import { RootContext } from '../src/'

const Todolist = () => {

  const state = useContext(RootContext)
  console.log("Todolist -> state", state)

  const renderItem = ({ item, index }) => {
      return(
          <View style={styles.content}>
              <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                  <View style={{flexDirection: 'column', padding: 10}}>
                    <Text>{ item.date }</Text>
                    <Text>{ item.title }</Text>
                  </View>
                  <View style={{justifyContent: 'center'}}>
                    <TouchableOpacity onPress={() => state.deleteTodo(item.index)}>
                        <Image style={styles.delete} source={require("./assets/icons/trash.png")} />
                    </TouchableOpacity>
                  </View>
              </View>
          </View>
      )
  }

  return (
    <View style={{paddingLeft: 10}}>
        <Text style={{paddingTop: 40, paddingBottom: 10}}>Masukan Todolist</Text>
        <View style={styles.containerCol}>
            <View style={styles.containerRow}>
                <TextInput
                style={styles.form}
                value={state.input}
                placeholder='input here'
                onChangeText={(value) => state.handleChangeInput(value)}
                />
                <View>
                    <TouchableOpacity
                    style={styles.addButton}
                    onPress={() => state.addTodo()}
                    >
                        <Text style={{fontSize:30,}}>+</Text>
                    </TouchableOpacity>
                </View>
            </View>
            <FlatList 
                data={state.todos}
                renderItem={renderItem}
            />
        </View>
    </View>
  )
}

const styles = StyleSheet.create({
    containerRow:{
        flexDirection: 'row',
        paddingBottom: 20,
    },
    containerCol: {
        flexDirection: 'column',
    },
    form: {
      borderWidth: 1,
      height: 45,
      width: '85%',
      marginRight: 5
    },
    addButton: {
      backgroundColor: '#1ed4eb',
      width: 45,
      height: 45,
      alignItems: 'center',
    },
    content: {
      width: '98%',
      height: 65,
      borderWidth: 4,
      borderRadius: 3,
      borderColor: '#ccb9b8',
      marginBottom: 10,
      flexDirection: 'column'
    },
    delete: {
      width: 30,
      height: 30,
      margin: 10
    },   
  });

export default Todolist
