import React from "react";
import {
  View,
  Text,
  Image,
  StyleSheet,
  TouchableOpacity,
  Animated,
  Dimensions,
  Easing,
} from "react-native";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    backgroundColor: "#fff",
  },
  logo: {
    width: 100,
    height: 100,
  },
  button: {
    backgroundColor: "yellow",
    padding: 10,
    marginTop: 50,
    alignSelf: "center",
  },
});

export default function Animation() {
  const xValue = new Animated.Value(0);
  const { width, heigth } = Dimensions.get("window");

  const rotation = () => {
    Animated.timing(xValue, {
      toValue: 1,
      duration: 3000,
    }).start(() => {
      rotation();
    });
  };

  const rotateData = xValue.interpolate({
    inputRange: [0, 3],
    outputRange: ["0deg", "360deg"],
  });

  const animate = () => {
    Animated.timing(xValue, {
      toValue: width - 100,
      duration: 1000,
      useNativeDriver: false,
      easing: Easing.cubic,
    }).start(() => {
      Animated.timing(xValue, {
        toValue: 0,
        duration: 1000,
        useNativeDriver: false,
        easing: Easing.linear,
      }).start(() => {
        animate();
      });
    });
  };
  return (
    <View style={styles.container}>
      <Animated.Image
        style={[
          styles.logo,
          { left: xValue, transform: [{ rotate: rotateData }] },
        ]}
        source={require("../assets/images/logobmka.png")}
      ></Animated.Image>
      <TouchableOpacity style={styles.button} onPress={animate}>
        <Text>Press Me</Text>
      </TouchableOpacity>
    </View>
  );
}
