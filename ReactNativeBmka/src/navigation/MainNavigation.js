import React from 'react'
import { createStackNavigator } from '@react-navigation/stack'

import {
    Splash,
    Login,
    Home,
    Animation,
    Rest,
} from '../screens/'

const Stack = createStackNavigator()

export default function MainNavigation() {
    return(
        <Stack.Navigator headerMode="none">
            <Stack.Screen name="Splash" component={ Splash } />
            <Stack.Screen name="Login" component={ Login } />
            <Stack.Screen name="Home" component={ Home } />
            <Stack.Screen name="Animation" component={ Animation } />
            <Stack.Screen name="Rest" component={ Rest } />
        </Stack.Navigator>
    )
}