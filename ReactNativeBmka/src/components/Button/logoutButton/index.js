import React from "react"
import { Text, TouchableOpacity, Alert } from 'react-native';

const LogoutButton = () => {
    return (
        <TouchableOpacity style={{
            marginTop:10,
            paddingTop:15,
            paddingBottom:15,
            marginLeft:30,
            marginRight:30,
            backgroundColor: '#CC1F1F',
            borderRadius:100,
            borderWidth: 1,
            borderColor: '#fff'
        }} onPress={() => Alert.alert("Logout")}>
        <Text style={{textAlign: 'center', color: '#fff', fontSize: 16}}>Logout</Text>
        </TouchableOpacity>
    )
}

export default LogoutButton