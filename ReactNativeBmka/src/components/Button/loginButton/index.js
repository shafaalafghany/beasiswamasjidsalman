import React from "react"
import { Text, TouchableOpacity, Alert } from 'react-native';

const LoginButton = () => {
    return (
        <TouchableOpacity style={{
            marginTop:10,
            paddingTop:15,
            paddingBottom:15,
            marginLeft:30,
            marginRight:30,
            backgroundColor: '#1F99CC',
            borderRadius:100,
            borderWidth: 1,
            borderColor: '#fff'
        }} onPress={() => console.log("tes")}>
        <Text style={{textAlign: 'center', color: '#fff', fontSize: 16}}>Masuk</Text>
        </TouchableOpacity>
    )
}

export default LoginButton