import React from "react"
import { TextInput, StyleSheet } from 'react-native'

const Form = ( ) => {
    return (
        <TextInput style={styles.form} />
    )
}

const styles = StyleSheet.create({
    form: {
        width: 330,
        height: 45,
        borderWidth: 1,
        borderColor: 'gray',
        borderRadius: 10,
        marginBottom: 15
    }
})

export default Form

