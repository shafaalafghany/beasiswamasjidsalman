import { blue } from "ansi-colors";
import React, { useState } from "react";
import {
  View,
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  TextInput
} from "react-native";
import { Form, LoginButton } from '../../components/'
import Axios from 'axios'
import Asyncstorage from '@react-native-community/async-storage'



const Login = ({navigation}) => {

  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')

  const saveToken = async (token) => {
    try {
      // console.log(token)
      await Asyncstorage.setItem("token", token)
    } catch (err) {
      console.log(err)
    }
  }

  const onLoginPress = () => {
    let data ={
      email: email,
      password: password
    }
    Axios.post('https://crowdfunding.sanberdev.com/api/auth/login', data, {
      timeout: 20000
    })
    .then((res) => {
      // console.log(res.data.data.token)
      saveToken(res.data.data.token)
      navigation.navigate("Home")
      setEmail('')
      setPassword('')      
    })
    .catch((err) => {
      console.log("Login -> err", err)
    })
  }

  return (
    <View styles={{backgroundColor: '#fff',}}>
      <View style={{margin: 10, alignItems: "center", }}>
        <View style={{height: '5%', backgroundColor: '#fff'}}>
          <Image style={styles.logo} source={require("../../assets/images/logobmka.png")} />
        </View>
      </View>
      <View style={styles.header}>
        <Text style={{fontSize: 20}}>Selamat Datang</Text>
        <Text style={{fontSize: 20}}>Pemimpin Umat</Text>
      </View>
      <View style={styles.content}>
        <Text style={{marginBottom: 10, fontSize: 16}}>Email</Text>
        <TextInput 
        style={styles.form}
        value={email}
        onChangeText={(email) => setEmail(email)}
        />
        <Text style={{marginBottom: 10, marginTop: 20, fontSize: 16}}>Password</Text>
        <TextInput 
        secureTextEntry
        style={styles.form}
        value={password}
        onChangeText={(password) => setPassword(password)}
        />
      </View>
      <View style={styles.additions}>
        <Text>
          Belum punya akun? 
          <Text style={{textDecorationLine: 'underline'}}>Daftar</Text>
        </Text>
        <Text style={{marginLeft:60, textDecorationLine: 'underline'}}>Lupa password?</Text>
      </View>
        {/* <LoginButton /> */}
        <View>
        <TouchableOpacity style={styles.button} onPress={() => onLoginPress()}>
          <Text style={{textAlign: 'center', color: '#fff', fontSize: 16}}>Masuk</Text>
        </TouchableOpacity>
      </View>

    </View>
  );
};

const styles = StyleSheet.create({
  logo: {
    width: 54,
    height: 57,
    margin: 30,
  },
  header: {
    width: 265,
    height: 200,
    paddingTop: 100,
    paddingLeft: 30,
  },
  form: {
    width: 330,
    height: 45,
    borderWidth: 1,
    borderColor: 'gray',
    borderRadius: 10,
    marginBottom: 15
  },
  content:{
    fontSize: 16,
    paddingLeft: 30,
  },
  additions:{
    paddingLeft: 30,
    flexDirection: 'row',
    paddingBottom:150,
  },
  button:{
    marginTop:10,
    paddingTop:15,
    paddingBottom:15,
    marginLeft:30,
    marginRight:30,
    backgroundColor:'#1F99CC',
    borderRadius:100,
    borderWidth: 1,
    borderColor: '#fff'
  }

});

export default Login;
