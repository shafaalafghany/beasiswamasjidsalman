import React, { useState, useEffect } from 'react'
import { View, Image, Text, StyleSheet, TextInput, TouchableOpacity, FlatList, ScrollView } from 'react-native'
import axios from 'axios'

export default function index() {
    const [title, setTitle] = useState("")
    const [value, setValue] = useState("")
    const [items, setItems] = useState([])

    useEffect(() => {
        getData()
    }, [])

    const sendData = () => {
        const data = {
            title,
            value,
        }
        console.log("data before send: ", data)
        axios.post('https://achmadhilmy-sanbercode.my.id/api/v1/news', data)
        .then(res => {
            console.log('res: ', res)
        })
        .catch(function(error){
            console.log(error.message)
        })
        setTitle("")
        setValue("")
        getData()
    }

    const getData = () => {
        axios.get('https://achmadhilmy-sanbercode.my.id/api/v1/news')
        .then(res => {
            // console.log('res get data: ', res)
            // console.log(res.data.data)
            setItems(res.data.data)
        })
    }

    const RenderItem = (item) => {
        return (
            <View style={styles.content}>
                <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                    <View style={{flexDirection: 'column', padding: 10}}>
                        <Text>{item.title}</Text>
                        <Text>{item.value}</Text>
                    </View>
                    <View style={{justifyContent: 'center'}}>
                        <TouchableOpacity>
                            <Image style={styles.deleteButton} source={require("../../assets/icons/trash.png")} />
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        )
    }

    return (
        <View style={{backgroundColor: '#fff'}}>
            <View>
                <Text style={styles.welcomeText}>Masukkan Data</Text>
                <TextInput 
                style={styles.form}
                placeholder="title"
                value={title}
                onChangeText={(value) => setTitle(value)}
                />
                <TextInput 
                style={styles.form}
                placeholder="value"
                value={value}
                onChangeText={(value) => setValue(value)}
                />
                <TouchableOpacity
                style={styles.button}
                onPress={sendData}>
                    <Text style={{textAlign: 'center', color: '#fff', fontSize: 16}}>Send</Text>
                </TouchableOpacity>
            </View>
            <View>
                <View style={styles.line} />
            </View>
            <ScrollView>
            <FlatList />
            {/* {items.data.map(item => {
                return (
                    <RenderItem title={item.title} value={item.value}/>
                )
            })} */}
            </ScrollView>
        </View>
    )
}

const styles = StyleSheet.create({
    welcomeText: {
        marginTop: 20,
        marginLeft: 10
    },
    form: {
        margin: 10,
        borderWidth: 1,
        borderRadius: 5,
        height: 50,
        // width: '80%'
    },
    button: {
        paddingTop:15,
        paddingBottom:15,
        marginLeft:30,
        marginRight:30,
        backgroundColor:'#1F99CC',
        borderRadius:5,
        borderWidth: 1,
        borderColor: '#fff'
    },
    line: {
        height: 2,
        backgroundColor: 'black',
        marginVertical: 20,
        margin: 10,
    },
    content: {
        margin: 10,
        height: 65,
        borderWidth: 4,
        borderRadius: 3,
        borderColor: '#ccb9b8',
        marginBottom: 10,
        flexDirection: 'column'
    },
    deleteButton: {
        width: 30,
        height: 30,
        margin: 10
    }
})