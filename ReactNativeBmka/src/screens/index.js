import Login from '../screens/Login'
import Splash from '../screens/Splash'
import Home from '../screens/Home'
import Animation from '../animation'
import Rest from '../screens/Rest'

export {
    Login,
    Splash,
    Home,
    Animation,
    Rest,
}