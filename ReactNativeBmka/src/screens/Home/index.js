import { blue } from "ansi-colors";
import React, { useEffect, useState } from "react";
import {
  View,
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  Alert,
} from "react-native";
import { LogoutButton } from '../../components/'
import Axios from 'axios'
import Asyncstorage from '@react-native-community/async-storage'

const Home = ({navigation}) => {

  const [name, setName] = useState('')

  useEffect(() => {
    async function getToken() {
      try {
        const token = await Asyncstorage.getItem("token")
        return getVenue(token)
      } catch(err) {
        console.log(err)
      }
    }
    getToken()
  }, [])


  const getVenue = (token) => {
    Axios.get('https://crowdfunding.sanberdev.com/api/profile/get-profile', {
      timeout: 20000,
      headers: {
        'Authorization' : 'Bearer' + token
      }
    })
    .then((res) => {
      // console.log(res.data.data.profile.name)
      setName(res.data.data.profile.name)
    })
    .catch((err) => {
      console.log("Home -> err", err)
    })
  }

  const onLogoutPress = async () => {
    try {
      await Asyncstorage.removeItem("token")
      navigation.navigate("Login")
    } catch(err) {
      console.log(err)
    }
  }

  return (
    <View>
      <View style={{ margin: 10, alignItems: "center" }}>
        <View style={{height: '5%'}}>
        <Image style={styles.logo} source={require("../../assets/images/logobmka.png")} />
        </View>
      </View>
      <View style={styles.header}>
        <Text style={{fontSize: 25}}>Home</Text>
        <Text style={{fontSize: 18,}}>Hello, {name}</Text>
      </View>
      <View style={styles.containerRow}>
        <View style={styles.containerCol}>
          <TouchableOpacity onPress={() => navigation.navigate("Rest")}>
            <View style={styles.kotak1}>
              <Image style={{paddingLeft: 20}} source={require("../../assets/icons/E-mail.png")} />
            </View>
          </TouchableOpacity>
          <Text style={{fontSize: 26, textAlign: 'center'}}>E-mail</Text>
        </View>
        <View style={styles.containerCol}>
          <TouchableOpacity onPress={() => Alert.alert("Account")}>
            <View style={styles.kotak2}>
              <Image style={{paddingLeft: 20}} source={require("../../assets/icons/Account.png")} />
            </View> 
          </TouchableOpacity>
          <Text style={{fontSize: 26, textAlign: 'center'}}>Account</Text>
        </View>
      </View>
      <View style={styles.containerRow}>
        <View style={styles.containerCol}>
          <TouchableOpacity onPress={() => Alert.alert("Gift")}>
            <View style={styles.kotak3}>
              <Image style={{paddingLeft: 20}} source={require("../../assets/icons/Gift.png")} />
            </View>
          </TouchableOpacity>
          <Text style={{fontSize: 26, textAlign: 'center'}}>Gift</Text>
        </View>
        <View style={styles.containerCol}>
          <TouchableOpacity onPress={() => navigation.navigate("Animation")}>
          <View style={styles.kotak4}>
            <Image style={{paddingLeft: 20}} source={require("../../assets/icons/Wishlist.png")} />
          </View>
          </TouchableOpacity>
          <Text style={{fontSize: 26, textAlign: 'center'}}>Whistlist</Text>
        </View>
      </View>
      <View style={{marginTop: 50}}>
        {/* <LogoutButton /> */}
        <TouchableOpacity style={styles.button} onPress={() => navigation.navigate("Login")}>
          <Text style={{textAlign: 'center', color: '#fff', fontSize: 16}}>Logout</Text>
        </TouchableOpacity>
      </View>      
    </View>
  );
};

const styles = StyleSheet.create({
  logo: {
    width: 54,
    height: 57,
    margin: 30,
  },
  header: {
    paddingTop: 70,
    paddingLeft: 30,
  },
  containerRow: {
    flexDirection: 'row',
    paddingLeft: 20
  },
  containerCol:{
    flexDirection: 'column',
  },
  kotak1:{
    backgroundColor: '#FF7B40',
    width: 125,
    height: 125,
    marginTop: 25,
    marginLeft: 25,
    marginRight: 25,
    marginBottom: 15,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10
  },
  kotak2:{
    backgroundColor: '#1F99CC',
    width: 125,
    height: 125,
    marginTop: 25,
    marginLeft: 25,
    marginRight: 25,
    marginBottom: 15,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10
  },
  kotak3:{
    backgroundColor: '#1CBC18',
    width: 125,
    height: 125,
    marginTop: 25,
    marginLeft: 25,
    marginRight: 25,
    marginBottom: 15,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10
  },
  kotak4:{
    backgroundColor: '#CC1F1F',
    width: 125,
    height: 125,
    marginTop: 25,
    marginLeft: 25,
    marginRight: 25,
    marginBottom: 15,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10
  },
  button:{
    marginTop:50,
    paddingTop:15,
    paddingBottom:15,
    marginLeft:30,
    marginRight:30,
    backgroundColor:'#CC1F1F',
    borderRadius:100,
    borderWidth: 1,
    borderColor: '#fff'
  }
});

export default Home;
