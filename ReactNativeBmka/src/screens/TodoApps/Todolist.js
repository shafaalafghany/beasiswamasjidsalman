import { blue } from "ansi-colors";
import React from "react";
import {
  View,
  Image,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity
} from "react-native";

const styles = StyleSheet.create({
  containerRow:{
      flexDirection: 'row',
      paddingBottom: 20,
  },
  containerCol: {
      flexDirection: 'column',
  },
  form: {
    borderWidth: 1,
    height: 45,
    width: '85%',
    marginRight: 5
  },
  plusButton: {
    backgroundColor: '#1ed4eb',
    width: 45,
    height: 45,
    alignItems: 'center',
  },
  content: {
    width: '98%',
    height: 65,
    borderWidth: 4,
    borderRadius: 3,
    borderColor: '#ccb9b8',
    marginBottom: 10
  },
  delete: {
    width: 30,
    height: 30,
    marginLeft: 150,
  },   
});

const Todolist = () => {
  return (
    <View style={{paddingLeft: 10}}>
        <Text style={{paddingTop: 40, paddingBottom: 10}}>Masukan Todolist</Text>
        <View style={styles.containerCol}>
            <View style={styles.containerRow}>
                <TextInput style={styles.form} placeholder='input here' />
                <View>
                    <TouchableOpacity style={styles.plusButton}>
                        <Text style={{fontSize:30,}}>+</Text>
                    </TouchableOpacity>
                </View>
            </View>
            <View style={styles.content}>
                <View style={{flexDirection: 'row'}}>
                    <View style={{flexDirection: 'column', padding: 10}}>
                        <Text>9/6/2020</Text>
                        <Text>Membuat login screens</Text>
                    </View>
                    <View style={{justifyContent: 'center'}}>
                        <TouchableOpacity>
                            <Image style={styles.delete} source={require("../../assets/icons/trash.png")} />
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
            <View style={styles.content}>
                <View style={{flexDirection: 'row'}}>
                    <View style={{flexDirection: 'column', padding: 10}}>
                        <Text>9/6/2020</Text>
                        <Text>Membuat login screens</Text>
                    </View>
                    <View style={{justifyContent: 'center'}}>
                        <TouchableOpacity>
                            <Image style={styles.delete} source={require("../../assets/icons/trash.png")} />
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        </View>
    </View>
  );
};

export default Todolist;
