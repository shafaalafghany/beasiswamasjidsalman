import React, { useEffect } from 'react';
import { View, Image, StyleSheet, Text } from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#fff'
  },
  logo: {
    width: 180,
    height: 190,
  },
});

const Splash = ({navigation}) => {
  useEffect(() => {
    setTimeout(() => {
      navigation.navigate("Login")
    }, 2000)
  }, [])

  return (
    <View style={styles.container}>
        <Image
          style={styles.logo}
          source={require('../../assets/images/logobmka.png')}
        />
      <View>

      </View>
    </View>
  );
}

export default Splash;